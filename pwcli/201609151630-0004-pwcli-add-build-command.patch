From bc4643809146395b4d8d551176458b8530beeab5 Mon Sep 17 00:00:00 2001
From: Kalle Valo <kvalo@codeaurora.org>
Date: Thu, 15 Sep 2016 13:48:45 +0300
Subject: [PATCH 4/5] pwcli: add build command

The build command is configured via in .git/pwcli/config:

build-command = make -j 8

Stdout is shown only one line at a time, stderr is shown in full after the
build has finished.

Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---

Copyright (c) 2016, The Linux Foundation.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 cmdtests/cmd-build.script           |   49 +++++++++++++++++++++++
 cmdtests/cmd-build.stdout           |   15 +++++++
 pwcli                               |   74 +++++++++++++++++++++++++++++++++++
 stubs/cmdtests/pwcli-wrapper.stdout |    1 +
 stubs/stubs.py                      |    1 +
 5 files changed, 140 insertions(+)
 create mode 100755 cmdtests/cmd-build.script
 create mode 100644 cmdtests/cmd-build.stdout

diff --git a/cmdtests/cmd-build.script b/cmdtests/cmd-build.script
new file mode 100755
index 000000000000..efa3b441260f
--- /dev/null
+++ b/cmdtests/cmd-build.script
@@ -0,0 +1,49 @@
+#!/usr/bin/env python
+#
+# Copyright (c) 2016, The Linux Foundation.
+# All rights reserved.
+#
+# Redistribution and use in source and binary forms, with or without
+# modification, are permitted provided that the following conditions are
+# met:
+#
+# 1. Redistributions of source code must retain the above copyright
+# notice, this list of conditions and the following disclaimer.
+#
+# 2. Redistributions in binary form must reproduce the above copyright
+# notice, this list of conditions and the following disclaimer in the
+# documentation and/or other materials provided with the distribution.
+#
+# 3. Neither the name of the copyright holder nor the names of its
+# contributors may be used to endorse or promote products derived from
+# this software without specific prior written permission.
+#
+# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
+# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
+# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
+# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
+# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
+# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
+# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
+# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+#
+
+import cmdtestlib
+
+def main():
+   ctxt = cmdtestlib.StubContext(start=True)
+   pwcli = ctxt.pwcli
+
+   pwcli.expect_prompt()
+   pwcli.sendline('build')
+
+   pwcli.expect_prompt()
+   pwcli.sendline('quit')
+
+   ctxt.stop_and_cleanup()
+
+if __name__ == "__main__":
+   main()
diff --git a/cmdtests/cmd-build.stdout b/cmdtests/cmd-build.stdout
new file mode 100644
index 000000000000..d01066a30205
--- /dev/null
+++ b/cmdtests/cmd-build.stdout
@@ -0,0 +1,15 @@
+Connecting to http://localhost:8000/
+User          : test (7477, 30052, 118371)
+Projects      : stub-test
+Tree          : data
+Branch        : test-branch
+New           : 1
+Review        : 2
+Upstream      : 0
+Deferred      : 1
+Total         : 4
+test-branch@data > build
+build
+                                                                 This is a warning
+Build failed with warnings
+test-branch@data > quit
diff --git a/pwcli b/pwcli
index d2eed85ab253..155c70a3cbda 100755
--- a/pwcli
+++ b/pwcli
@@ -46,6 +46,7 @@ import email.header
 import smtplib
 import pprint
 import re
+import timeit
 
 PWCLI_EDIT_FILE = '.pwcli-edit'
 
@@ -126,6 +127,17 @@ def parse_list(entries):
 
    return ids
 
+class Timer():
+
+   def start(self):
+      self.start_time = timeit.default_timer()
+
+   def stop(self):
+      self.end_time = timeit.default_timer()
+
+   def get_seconds(self):
+      return self.end_time - self.start_time
+
 class GitError(Exception):
 
    def __str__(self):
@@ -872,6 +884,53 @@ class PWCLI():
       self.print_header(PATCH_STATE_DEFERRED, deferred)
       self.print_header('Total', total)
 
+   def run_build_script(self):
+      cmd = self.build_command
+
+      if not cmd:
+         # build command is not specified, skip the build
+         return
+
+      if self.dry_run:
+            self.output('Dry-run: %s' % str(cmd))
+            return
+
+      empty_line = '                                                                 '
+      stdoutdata = ''
+      stderrdata = ''
+
+      self.timer.start()
+      p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
+
+      lines = iter(p.stdout.readline, '')
+      for line in lines:
+         stdoutdata += line
+         print '\r%s\r%s' % (empty_line, line.replace('\n', '')),
+
+      stderrdata = p.stderr.read()
+
+      p.stdout.close()
+      p.stderr.close()
+      returncode = p.wait()
+
+      self.timer.stop()
+
+      logger.debug('Build \'%s\' done in %.1fs: (%r, %r, %r)' %
+                   (cmd, self.timer.get_seconds(), returncode,
+                    stdoutdata, stderrdata))
+
+      # empty the line from "status" messages
+      print '\r%s\r' % (empty_line),
+
+      if returncode != 0:
+         self.output(stderrdata.strip())
+         self.output('Build failed: %d' % (returncode))
+      elif len(stderrdata) > 0:
+         self.output(stderrdata.strip())
+         self.output('Build failed with warnings')
+      else:
+         self.output('Build succesful')
+
    def get_pending_branch_patches(self):
       patches = []
 
@@ -1377,6 +1436,11 @@ class PWCLI():
 
       self.show_info()
 
+   def cmd_build(self, args):
+      logger.debug('cmd_build(args=%s)' % repr(args))
+
+      self.run_build_script()
+
    def cmd_project(self, args):
       logger.debug('cmd_project(args=%r)' % repr(args))
 
@@ -1508,6 +1572,9 @@ class PWCLI():
       subparsers.add_parser('info').set_defaults(func=self.cmd_info,
                                                  help='show various patchwork statistics')
 
+      subparsers.add_parser('build').set_defaults(func=self.cmd_build,
+                                                 help='run build script and show results')
+
       while True:
          prompt = ''
 
@@ -1551,6 +1618,7 @@ class PWCLI():
       return cmd
 
    def __init__(self):
+      self.timer = Timer()
 
       if 'GIT_DIR' in os.environ:
          self.git_dir = os.environ['GIT_DIR']
@@ -1603,6 +1671,12 @@ class PWCLI():
       self.url = self.config.get('general', 'url')
       self.username = self.config.get('general', 'username')
       self.password = self.config.get('general', 'password')
+
+      if self.config.has_option('general', 'build-command'):
+         self.build_command = self.config.get('general', 'build-command')
+      else:
+         self.build_command = None
+
       self.project_names = self.config.get('general', 'project')
 
       self.project_names = self.project_names.split()
diff --git a/stubs/cmdtests/pwcli-wrapper.stdout b/stubs/cmdtests/pwcli-wrapper.stdout
index 1cb962e16f8b..0af90e6c1999 100644
--- a/stubs/cmdtests/pwcli-wrapper.stdout
+++ b/stubs/cmdtests/pwcli-wrapper.stdout
@@ -3,5 +3,6 @@ project = stub-test
 password = password
 username = test
 url = http://localhost:8000/
+build-command = echo This is a warning >&2
 
 
diff --git a/stubs/stubs.py b/stubs/stubs.py
index e3d7d06d48d9..a534ef3474c5 100644
--- a/stubs/stubs.py
+++ b/stubs/stubs.py
@@ -335,6 +335,7 @@ class PwcliWrapper():
         self.config.set(general, 'password', 'password')
         self.config.set(general, 'username', 'test')
         self.config.set(general, 'url', 'http://localhost:8000/')
+        self.config.set(general, 'build-command', 'echo This is a warning >&2')
 
         if stgit:
             self.config.set(general, 'pending_mode', 'stgit')
-- 
1.7.9.5

