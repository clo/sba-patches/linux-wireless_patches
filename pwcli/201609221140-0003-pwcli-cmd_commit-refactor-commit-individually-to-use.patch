From b5fce8dbb0ef94d84deec90e95960dea3665bf2b Mon Sep 17 00:00:00 2001
From: Kalle Valo <kvalo@codeaurora.org>
Date: Wed, 21 Sep 2016 21:06:16 +0300
Subject: [PATCH 3/8] pwcli: cmd_commit(): refactor commit individually to use
 show_choose_list()

Less code duplication.

Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---

Copyright (c) 2016, The Linux Foundation.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 pwcli |   41 +++++++++++++++++++++++------------------
 1 file changed, 23 insertions(+), 18 deletions(-)

diff --git a/pwcli b/pwcli
index e81e24a01775..291b88699c2f 100755
--- a/pwcli
+++ b/pwcli
@@ -791,10 +791,20 @@ class PWCLI():
 
       return result.strip('\n')
 
-   def show_choose_list(self, table):
+   def show_choose_list(self, table, prefix=None):
 
       valid = {}
 
+      if not prefix:
+         prefix = ''
+
+      # for convenience convert list to a dict
+      if isinstance(table, list):
+         copy = table
+         table = collections.OrderedDict()
+         for c in copy:
+            table[c] = c
+
       for name in table.keys():
          # find the one char shortcut
          match = re.search('([A-Z])', name)
@@ -804,7 +814,7 @@ class PWCLI():
          valid[shortcut] = table[name]
 
       while True:
-         choices = '%s? ' % ('/'.join(table.keys()))
+         choices = '%s%s? ' % (prefix, '/'.join(table.keys()))
          answer = self.input(choices)
          answer = answer.lower()
 
@@ -1147,31 +1157,26 @@ class PWCLI():
 
       applied = []
       for patch in patches:
+
+         # edit can be chosen multiple times, that's why the loop
          while commit_individually:
             # FIXME: in stgit mode we should show the commit log from
             # pending branch
             self.show_patch(patch)
 
-            answer = None
-
-            while True:
-               answer = self.input('[%d/%d] Commit/Edit/Abort? ' % (len(applied) + 1,
-                                                                    len(patches)))
-               answer = answer.lower()
+            ANSWER_COMMIT = 'Commit'
+            ANSWER_EDIT = 'Edit'
+            ANSWER_ABORT = 'Abort'
 
-               valid = ['c', 'e', 'a']
-
-               if answer in valid:
-                  break
-
-               self.output('Unknown choise \'%s\', valid choises are: %s' % (answer,
-                                                                          ', '.join(valid)))
+            answer = self.show_choose_list([ANSWER_COMMIT, ANSWER_EDIT, ANSWER_ABORT],
+                                           prefix='[%d/%d] ' % (len(applied) + 1,
+                                                                len(patches)))
 
-            if answer == 'a':
+            if answer == ANSWER_ABORT:
                self.output('Commit command aborted')
                return
 
-            if answer == 'e':
+            if answer == ANSWER_EDIT:
                mbox = patch.get_mbox()
                new_mbox = self.edit_file(mbox)
 
@@ -1180,7 +1185,7 @@ class PWCLI():
 
                continue
 
-            if answer == 'c':
+            if answer == ANSWER_COMMIT:
                # continue with commit process
                break
 
-- 
1.7.9.5

