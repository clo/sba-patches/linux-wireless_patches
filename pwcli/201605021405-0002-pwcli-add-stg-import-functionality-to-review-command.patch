From 72aa71b04c22e2261e51846cfb5cd184122a3fc8 Mon Sep 17 00:00:00 2001
From: Kalle Valo <kvalo@codeaurora.org>
Date: Thu, 28 Apr 2016 20:43:33 +0300
Subject: [PATCH 2/5] pwcli: add stg import functionality to review command

This adds a new pending_mode configuration and two branches:

pending_mode = stgit
pending_branch = pending
main_branch = ath-next

Then using the review command command pwcli imports the patch to the pending
branch and adds Patchwork-Id tag. The user can after that freely edit the patch.

The part of committing the patch from pending branch to the main branch is not
yet implemented.

Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---

Copyright (c) 2016, The Linux Foundation.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 pwcli                   |  126 +++++++++++++++++++++++++++++++++++++++++++++--
 unittests/test_patch.py |   44 ++++++++++++++++-
 2 files changed, 165 insertions(+), 5 deletions(-)

diff --git a/pwcli b/pwcli
index 76c1a6eb1593..6a57c3255067 100755
--- a/pwcli
+++ b/pwcli
@@ -44,6 +44,7 @@ import email
 import email.mime.text
 import smtplib
 import pprint
+import re
 
 PWCLI_EDIT_FILE = '.pwcli-edit'
 
@@ -121,6 +122,30 @@ class GitError(Exception):
    def __init__(self, msg):
       self.msg = msg
 
+class Stg():
+
+   def import_patch(self, mbox):
+      cmd = ['stg', 'import', '--mbox', '--sign']
+
+      if self.dry_run:
+            self.output('Dry-run: %s' % str(cmd))
+            return
+
+      logger.debug('stg.import_patch(): %s' % (mbox))
+
+      p = subprocess.Popen(cmd, stdin = subprocess.PIPE)
+      p.communicate(mbox)
+
+      ret = p.returncode
+      logger.debug('%s returned: %s' % (cmd, ret))
+
+      if ret != 0:
+         raise GitError('%s failed: %s' % (cmd, ret))
+
+   def __init__(self, dry_run, output):
+      self.dry_run = dry_run
+      self.output = output
+
 class Git():
 
    def log_oneline(self, max_count):
@@ -154,6 +179,10 @@ class Git():
 
       return None
 
+   def checkout(self, branch):
+      cmd = ['git', 'checkout', branch]
+      subprocess.check_output(cmd)
+
    def am(self, mbox):
       cmd = ['git', 'am', '-s', '-3']
 
@@ -223,6 +252,31 @@ class Patch():
 
       return utf8(self.mbox)
 
+   # removes all extra '[ ]' tags _before_ the actual title
+   def clean_subject(self, subject):
+      # Note: '.*?' is a non-greedy version of '.*'
+
+      return re.sub(r'^\s*(\[.*?\]\s*)*', '', subject)
+
+   def get_mbox_for_stgit(self):
+      msg = self.get_email()
+
+      payload = msg.get_payload()
+
+      # add Patchwork-Id with s/^---\n/Patchwork-Id: 1001\n---\n
+      id_line = 'Patchwork-Id: %s\n---\n' % (self.get_id())
+      payload = re.sub(r'\n---\n', id_line, payload)
+      msg.set_payload(payload)
+
+      subject = self.clean_subject(msg['Subject'])
+      msg.replace_header('Subject', subject)
+
+      # Add a From header with unixfrom so that this is valid mbox
+      # format, strangely patchwork doesn't add it.
+      mbox = msg.as_string(unixfrom=True)
+
+      return mbox
+
    def set_mbox(self, mbox):
       if self.dry_run:
          logger.debug('dry-run: %s().set_mbox(%s)' % (self, mbox))
@@ -784,8 +838,20 @@ class PWCLI():
       for i in indexes:
          selected_patches.append(self.patches[i])
 
+      # retrieve all mbox files in one for "smoother user experience"
+      i = 1
+      for patch in selected_patches:
+         print '\rRetrieving patches (%d/%d)' % (i, len(selected_patches)),
+         sys.stdout.flush()
+
+         patch.get_mbox()
+         i += 1
+
+      # force a newline so that new text starts from it's own line
+      self.output('')
+
       for patch in selected_patches:
-         self.output('[%-3d] %s %s' % (i, patch.get_id(), patch.get_name()))
+         self.output('%s %s' % (patch.get_id(), patch.get_name()))
 
       self.output('------------------------------------------------------------')
 
@@ -795,10 +861,31 @@ class PWCLI():
       if answer != 'y':
          return
 
-      for patch in selected_patches:
+      applied = []
+      skipped = []
+      if self.pending_mode == 'stgit':
+         # git checkout self.pending_branch
+         self.git.checkout(self.pending_branch)
+         
+         for patch in selected_patches:
+            # stg import --sign
+            # FIXME: catch GitError
+            try:
+               self.stg.import_patch(patch.get_mbox_for_stgit())
+               applied.append(patch)
+            except GitError as e:
+               self.output('Failed to apply patch: %s' % e)
+               skipped.append(patch)
+
+      else:
+         # stgit disabled, no need to apply any patches
+         applied = selected_patches
+
+      for patch in applied:
          patch.set_state_name('Under Review')
 
-      self.output('%d patches set to Under Review' % len(selected_patches))
+      # FIXME: print summary of applied and skipped patches (with totals)
+      self.output('%d patches set to Under Review, %d skipped' % (len(applied), len(skipped)))
 
    def cmd_reply(self, args):
       logger.debug('cmd_reply(args=%s)' % repr(args))
@@ -968,14 +1055,45 @@ class PWCLI():
       else:
          self.default_filter_delegated = True
 
+      pending_modes = ['disabled', 'stgit']
+
+      # defaults
+      self.pending_mode = 'disabled'
+      self.pending_branch = None
+      self.main_branch = None
+
+      if self.config.has_option('general', 'pending_mode'):
+         self.pending_mode = self.config.get('general', 'pending_mode').lower()
+
+      if self.pending_mode not in pending_modes:
+         self.output('Invalid stgit.mode "%s"' % self.pending_mode)
+         sys.exit(1)
+
+      if self.config.has_option('general', 'pending_branch'):
+         self.pending_branch = self.config.get('general', 'pending_branch').lower()
+
+      if self.config.has_option('general', 'main_branch'):
+         self.main_branch = self.config.get('general', 'main_branch').lower()
+
+      if self.pending_mode == 'stgit':
+         if self.pending_branch == None:
+            self.output('general.pending_branch not set')
+            sys.exit(1)
+
+         if self.main_branch == None:
+            self.output('general.main_branch not set')
+            sys.exit(1)
+
       # read settings from environment variables
       if 'EDITOR' in os.environ:
          self.editor = os.environ['EDITOR']
       else:
          self.editor = DEFAULT_EDITOR
 
-      # read settings from git
       self.git = Git(self.dry_run, self.output)
+      self.stg = Stg(self.dry_run, self.output)
+
+      # read settings from git
       self.fullname = self.git.get_config('user.name')
       self.email = self.git.get_config('user.email')
       self.smtp_host = self.git.get_config('sendemail.smtpserver')
diff --git a/unittests/test_patch.py b/unittests/test_patch.py
index 51d188ae14bf..3328c225a9b1 100644
--- a/unittests/test_patch.py
+++ b/unittests/test_patch.py
@@ -34,6 +34,7 @@
 import unittest
 import mock
 import email
+import re
 
 import pwcli
 
@@ -47,7 +48,7 @@ FAKE_ATTRIBUTES = {
 
 TEST_MBOX = 'Content-Type: text/plain; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\nSubject: [1/7] foo\nFrom: Dino Dinosaurus <dino@example.com>\nX-Patchwork-Id: 12345\nMessage-Id: <11111@example.com>\nTo: list@example.com\nDate: Thu,  10 Feb 2011 15:23:31 +0300\n\nFoo commit log. Ignore this text\n\nSigned-off-by: Dino Dinosaurus <dino@example.com>\n\n---\nFIXME: add the patch here\n'
 
-class TestGit(unittest.TestCase):
+class TestPatch(unittest.TestCase):
     @mock.patch('pwcli.PWCLI')
     def test_attributes(self, pw):
         attributes = FAKE_ATTRIBUTES
@@ -78,5 +79,46 @@ class TestGit(unittest.TestCase):
         self.assertFalse('Cc' in reply)
         self.assertEqual(reply['Subject'], 'Re: [1/7] foo')
 
+    def test_get_mbox_for_stgit(self):
+        attributes = FAKE_ATTRIBUTES
+        patch = pwcli.Patch(None, attributes, False)
+
+        patch.get_mbox = mock.Mock(return_value=TEST_MBOX)
+
+        mbox = patch.get_mbox_for_stgit()
+        msg = email.message_from_string(mbox)
+
+        # Check that the first line matches mbox format
+        #
+        # It would be nice to mock datetime.datetime so that we would
+        # not have to skip the date from the first line but I didn't
+        # figure out how to do that, without a library like freezegun.
+        firstline = mbox.splitlines()[0]
+        self.assertTrue(firstline.startswith('From nobody '))
+
+        self.assertEqual(msg['Subject'], 'foo')
+
+        # check that Patchwork-Id is set
+        id_line = r'\nPatchwork-Id: %s\n' % (attributes['id'])
+        search = re.search(id_line, msg.get_payload())
+        self.assertTrue(search != None)
+
+    def test_clean_subject(self):
+        attributes = FAKE_ATTRIBUTES
+        patch = pwcli.Patch(None, attributes, False)
+
+        c = patch.clean_subject
+
+        self.assertEqual(c('[] One two three four'), 'One two three four')
+        self.assertEqual(c('[1/100] One two three four'), 'One two three four')
+        self.assertEqual(c('[PATCH 1/100] One two three four'),
+                         'One two three four')
+        self.assertEqual(c('[PATCH RFC 14/14] foo: bar koo'), 'foo: bar koo')
+        self.assertEqual(c('[RFC] [PATCH 14/99]   foo: bar koo'), 'foo: bar koo')
+        self.assertEqual(c('bar: use array[]'), 'bar: use array[]')
+        self.assertEqual(c('[PATCH] bar: use array[]'), 'bar: use array[]')
+        self.assertEqual(c('[] [A] [B] [   PATCH 1/100000  ] bar: use [] in array[]'),
+                         'bar: use [] in array[]')
+
 if __name__ == '__main__':
     unittest.main()
-- 
1.7.9.5

