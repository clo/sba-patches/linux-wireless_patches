From 7e08685efd8be59c4b7a1cd22029a47896445ab4 Mon Sep 17 00:00:00 2001
From: Kalle Valo <kvalo@codeaurora.org>
Date: Thu, 25 May 2017 10:50:57 +0300
Subject: [PATCH 04/14] pwcli: show progress bar when importing patches with
 stg

It's quite slow so better to indicate to the user what's happening.

Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---

Copyright (c) 2017, The Linux Foundation.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 cmdtests/cmd-review-stg-build-failure-accept.stdout |  1 +
 cmdtests/cmd-review-stg-conflict.stdout             |  1 +
 cmdtests/cmd-review-stg-multiple-fail.stdout        |  1 +
 cmdtests/cmd-review-stg-single.stdout               |  1 +
 pwcli                                               | 10 ++++++++++
 5 files changed, 14 insertions(+)
 mode change 100755 => 100644 cmdtests/cmd-review-stg-build-failure-accept.stdout

diff --git a/cmdtests/cmd-review-stg-build-failure-accept.stdout b/cmdtests/cmd-review-stg-build-failure-accept.stdout
old mode 100755
new mode 100644
index 5c8f2a1b9584..8a554d3acf48
--- a/cmdtests/cmd-review-stg-build-failure-accept.stdout
+++ b/cmdtests/cmd-review-stg-build-failure-accept.stdout
@@ -36,6 +36,7 @@ review 1
 ------------------------------------------------------------
 Apply 1 patches to the pending branch? [Apply/Skip/aBort] a
 a
+Importing patches (1/1)
                                                                    CC foo.c                                                                   CC bar.c                                                                   CC aaa.c                                                                   CC bbb.c                                                                 
 Build failed: 1
 Under review/Changes requested/Rejected/New/Deferred/Superseded/aWaiting upstreamm/not aPplicable/rFc/aBort? u
diff --git a/cmdtests/cmd-review-stg-conflict.stdout b/cmdtests/cmd-review-stg-conflict.stdout
index 14e2691c9c5d..1083d297beb1 100644
--- a/cmdtests/cmd-review-stg-conflict.stdout
+++ b/cmdtests/cmd-review-stg-conflict.stdout
@@ -36,6 +36,7 @@ review 1
 ------------------------------------------------------------
 Apply 1 patches to the pending branch? [Apply/Skip/aBort] a
 a
+Importing patches (1/1)
 Failed to apply patch: ['stg', 'import', '--mbox', '--sign'] failed: 2
 error: patch failed: drivers/net/wireless/foo/core.c:1714
 error: drivers/net/wireless/foo/core.c: patch does not apply
diff --git a/cmdtests/cmd-review-stg-multiple-fail.stdout b/cmdtests/cmd-review-stg-multiple-fail.stdout
index fb31ea76e1e3..533d16fd4958 100644
--- a/cmdtests/cmd-review-stg-multiple-fail.stdout
+++ b/cmdtests/cmd-review-stg-multiple-fail.stdout
@@ -40,6 +40,7 @@ review 1-5
 ------------------------------------------------------------
 Apply 5 patches to the pending branch? [Apply/Skip/aBort] a
 a
+Importing patches (1/5)Importing patches (2/5)Importing patches (3/5)
 Failed to apply patch: ['stg', 'import', '--mbox', '--sign'] failed: 2
 error: patch failed: drivers/net/wireless/foo/core.c:1714
 error: drivers/net/wireless/foo/core.c: patch does not apply
diff --git a/cmdtests/cmd-review-stg-single.stdout b/cmdtests/cmd-review-stg-single.stdout
index 8af85b2768d4..10eb643b46b9 100644
--- a/cmdtests/cmd-review-stg-single.stdout
+++ b/cmdtests/cmd-review-stg-single.stdout
@@ -36,6 +36,7 @@ review 1
 ------------------------------------------------------------
 Apply 1 patches to the pending branch? [Apply/Skip/aBort] a
 a
+Importing patches (1/1)
                                                                    CC foo.c                                                                   CC bar.c                                                                   CC aaa.c                                                                   CC bbb.c                                                                 Build successful
 Under review/Changes requested/Rejected/New/Deferred/Superseded/aWaiting upstreamm/not aPplicable/rFc/aBort? u
 u
diff --git a/pwcli b/pwcli
index 89d5a0f3c251..90ed67ae05b0 100755
--- a/pwcli
+++ b/pwcli
@@ -1721,9 +1721,16 @@ class PWCLI():
             self.git.checkout(self.pending_branch)
          
             try:
+               i = 1
                for patch in patches:
+                  self.output('\rImporting patches (%d/%d)' % (i, len(patches)),
+                              newline=False)
                   self.stg.import_patch(patch.get_mbox_for_stgit())
                   applied.append(patch)
+                  i += 1
+
+               # newline to clear the "progress bar"
+               self.output('')
                   
                # run a build test
                builder = self.run_build_script()
@@ -1732,6 +1739,9 @@ class PWCLI():
                   faillog += builder.stderrdata
 
             except GitError as e:
+               # newline to clear the "progress bar"
+               self.output('')
+
                self.output('Failed to apply patch: %s' % e)
                self.output('%s' % e.log)
                faillog += e.log
-- 
2.7.4

